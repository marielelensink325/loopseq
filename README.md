# LOOPseq
A Clone Verification Tool

## Quickstart
This package is designed around the snakemake workflow system and we recommend using conda
for installing the required packages. Given a user specified “components.fasta” file with 
the sequences you inserted via the uLoop reaction and a “reads.ont.fastq” file with sequencing
results from your reaction, download and run this pipeline using the following series of commands

```
git clone https://gitlab.com/marielelensink325/loopseq.git
conda env create -n loopseq_analysis -f loopseq/requirements.yml
conda activate loopseq_analysis
snakemake --snakefile loopseq/snakefile -d outdir -j 2 --config reads=reads.ont.fastq gblocks=components.fasta inserts=3
```

It is important to remember that you execute the snakemake command from the same directory where the snakefile is located.

## Detailed Usage
LOOPseq requires 2 files inputted by the user via the command line. 
The first is the fastq raw read output from the sequencing run (ex: reads=reads.ont.fastq). 
The second input file is a fasta formatted file consisting of all of the possible insertion 
components and their sequences used in the assembly experiment (ex: gblocks=components.fasta). 

There are 2 other input files used but these are internal to the program. One is a fasta file 
containing the standard backbone gene sequences used in loop assembly, and the other is a fasta 
file containing standard selectable marker sequences. If for some reason the user has incorporated
alternative sequences to the ones provided or used a different set of backbone genes and selectable 
markers entirely, these files may be altered or swapped manually. 

The last config option pertains to the number of insertion positions incorporated in the LOOP assembly
reaction. The versatile nature of LOOP assembly methods allows for great variation in the number of possible
insertions into the plasmid backbone, so this parameter must be set in the command line  so the algorithm can
adjust accordingly (ex: insert =3).

command line example:

`snakemake --snakefile loopseq/snakefile -d outdir -j 2 --config reads=reads.ont.fastq gblocks=components.fasta inserts=3`

## Output Files
LOOPseq will create an output directory that contains the following files:

| File Name | Description |
| ------ | ------ |
| Loopseq_viz.html | An html page containing interactive graphics displaying the composition and frequency of full length plasmid|
| Plasmid_types.counts.tsv | A list of unique plasmid types and their frequency. Formatted as a table with each insertion component listed per column and the number of times that specific plasmid was found in the final column  |
| full_length_reads.fasta | FastA file containing all of the filtered, full length plasmid sequences |
| Read_types.tsv | A table containing the sequence ID of each full length read and its corresponding insertion component annotations  |
| SummaryStats.tsv | A table containing statistics/quantitative information on read output available for further analysis  |
| *blast.tbl | Three tables from blast - one for backbones, selectable markers, and insertion components - containing annotations for each read as well as supplemental information on each blast hit (description below) |

### Loopseq_viz.html Description
![Pie chart](images/Screen_Shot_2020-07-06_at_12.33.02_PM.png)

*Figure 1  displays the proportion of unique plasmid types (determined by combination of inserted sequences) a hover tool is included in place of a legend for a more condensed display of information*

![Legend](images/Screen_Shot_2020-07-06_at_12.33.17_PM.png)

*Figure 2 displays the legend accompanying the heat map (displayed below). There is a seperate legend for each insertion position.*

![Heat Map](images/Screen_Shot_2020-07-06_at_12.33.49_PM.png)

*Figure 3 is a heat map depicting each full length read and its corresponding insertion sequences. Each horizontal row represents a single read. Each column represents one of the inserted sequences on that read (in order). The number of columns will correspond with the number of insertion positions determined by the user.*

### Blast Tables Description
| Column | Content |
| ------ | ------ |
| 1 | Read ID (Subject) |
| 2 | Inserted Sequence (Query)  |
| 3 | Subject Strand |
| 4 | Subject Length |
| 5 | Subject Start |
| 6 | Subject End |
| 7 | Query Length |
| 8 | Query Start |
| 9 | Query End |
| 10 | ALignment Length |
| 11 | E Value |
| 12 | Percent Identity |

## Motivation
Cloning is common practice in a majority of labs in the world and determination of the sequences that were cloned is required. As synthetic biology advances, constructs get increasingly complex , often containing repeat sequences, and can be quite large. This makes sequencing with short read technologies impossible because reading through and assembling repeats becomes labor intensive, especially  in the design of multiple primers.
    
In contrast, long read sequencing technologies provide necessary advantages that make sequencing highly repetitive, highly similar plasmids more accurate and less laborious. For example, sequencing plasmids with Oxford Nanopore Technologies can be performed on repeat rich sequences over 1 kb to determine the exact copy number within a cloned sequence. Furthermore, the genesis of TypeII assembly methods has made refactoring pathways for generating novel compounds commonplace. It has also made cloning complex sequences with directionality simple, however, sequence analysis of these complex constructs is laborious or even impossible with short read technologies. 

Because of this, there is a need for new bioinformatics tools to analyze plasmid sequencing output from long read technologies such as that from Oxford Nanopore. Our program, LOOPseq, generates graphs and table summaries for observing the complex combinatorial constructs generated with uLOOP assembly methods.  These tools will help smaller labs rapidly analyze complex constructs rapidly within their own labs.

## Features
LOOPseq determines full length plasmids by analyzing the composition of inserted components via user defined settings such as the number of insertion positions as well as what components should be present. LOOPseq does this by using Blast to annotate fastq raw read output using standard backbone and selectable marker sequences as well as a user provided database of all possible insertion components. The program then runs an overlap analysis and quality control of each annotated read and filters for full length plasmid sequences. 
After annotating and determining full length plasmids, LOOPseq then provides a diverse set of output files for a wide array of analysis. 

## Credits
Nolan Hartwick (co-developer)

Brad Abramson, PhD (postdoc)

Todd Michael, PhD (principal investigator)

The Salk Institute
