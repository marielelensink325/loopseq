from math import pi

import pandas

from bokeh.io import output_file, show
from bokeh.palettes import Category20c
from bokeh.plotting import figure, save 
from bokeh.transform import cumsum
from bokeh.models import Title
import colorcet as cc

def load_read_types(fpath):
    ret = pandas.read_csv(fpath, sep="\t")
    ret.columns = ["components", "freq"]
    ret.components = ret.components.apply(eval).apply(list).str[1:]
    comp_lens = ret.components.apply(len)
    if(any(comp_lens[0] != l for l in comp_lens)):
        raise ValueError("unequal length components detected")
    return ret

def pie_columns(data):
    data['angle'] = data['freq']/data['freq'].sum() * 2*pi
    data['color'] = cc.glasbey[:len(data.index)]
    return data

def main():
    #read_counts = "/Users/marielelensink/loopseqtesting/HEATMAP_RXN3Barcode3_Results/plasmid_types.counts.tsv"
    read_counts = snakemake.input[0]
    outpath = snakemake.output[0]
    df = load_read_types(read_counts)
    newdf = pie_columns(df)
    #print(newdf)
    

    p = figure(plot_height=600, title="Frequency of Unique Plasmid Types", toolbar_location=None,
           tools="hover", tooltips="@components: @freq", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', source=newdf)

    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None
    p.add_layout(Title(text="Description: Pie chart representing the frequency of each unique plasmid type <br> in relation to the total number of full length plasmids detected.", align="left",text_font_size='8pt'), "below")
    output_file(outpath)
    save(p)

if(__name__ == "__main__"):
    main()