import pandas
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource
from bokeh.layouts import column, gridplot, row
from bokeh.models import HoverTool, Title
from bokeh.transform import cumsum
from bokeh.resources import CDN
from bokeh.embed import json_item
import colorcet as cc
from math import pi
import json

html_template = """
<!DOCTYPE html>
<html lang="en">
<head>
  {cdn}
</head>
<body>
  <div id=header><h1>Plasmid Analysis</h1></div>
  <div id="pie_chart"></div>
  <div id="pie_chart_desc">Pie chart showing percent composition of full length plasmids according to their unique sequence in relation to total number of full length plasmids detected. (Hover cursor over figure to see information on plasmid type and count)  </div>
  <div id="heatmap"></div>
  <div id="heatmap_desc">Heatmap depicting frequency and configuration of full length plasmids. Each row represents a single read. Each column represents an insertion position and has a corresponding legend. Reads are clustered according to frequency (descending by number of plasmid type.)  </div>
  <script>
    pie_data = {pie_chart_json}
    Bokeh.embed.embed_item(pie_data, "pie_chart")
  </script>
  <script>
    heatmap_data = {heatmap_json}
    Bokeh.embed.embed_item(heatmap_data, "heatmap")
  </script>
</body>
"""


def load_read_types(fpath):
    ret = pandas.read_csv(fpath, sep="\t")
    ret.columns = ["components", "freq"]
    ret.components = ret.components.apply(eval).apply(list).str[1:]
    comp_lens = ret.components.apply(len)
    if(any(comp_lens[0] != l for l in comp_lens)):
        raise ValueError("unequal length components detected")
    return ret


def pie_columns(data):
    data = data.copy()
    data['angle'] = data['freq']/data['freq'].sum() * 2*pi
    data['color'] = cc.glasbey[:len(data.index)]
    return data


def build_pie_chart(counts):
    newdf = pie_columns(counts)
    p = figure(plot_height=600, title="Frequency of Unique Plasmid Types", toolbar_location=None,
           tools="hover", tooltips="@components: @freq", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', source=newdf)

    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None
    return json_item(p)


def counts_to_flat(counts):
    comp_positions = len(counts.components.iloc[0])
    reads_df = pandas.DataFrame(
        {
            f"block_{i}": [
                blk
                for blk, c in zip(counts.components.str[i], counts.freq)
                for j in range(c)
            ]
            for i in range(comp_positions)
        }
    )
    return reads_df


def get_colors(reads_df):
    colors = [
        "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78",
        "#2ca02c", "#98df8a", "#d62728", "#ff9896",
        "#9467bd", "#c5b0d5", "#8c564b", "#c49c94",
        "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7",
        "#bcbd22", "#dbdb8d", "#17becf", "#9edae5"
    ]
    color_map = {
        pos: {
            gblock: colors[i]
            for i, gblock in enumerate(reads_df[pos].unique())
        }
        for pos in reads_df.columns
    }
    return color_map


def build_legends(reads_df, color_map):
    legends = []
    for pos, mapping in color_map.items():
        f = figure(
            plot_width = 300,
            plot_height=300,
            title=f"{pos} legend",
            y_range = [" ".join(i) for i in mapping],
            x_range = ["0"]
        )
        temp = pandas.DataFrame({
            "dummy_pos": "0",
            "gblock": [" ".join(i) for i in mapping.keys()],
            "color": list(mapping.values())
        })
        lsource = ColumnDataSource(temp)
        f.rect("dummy_pos", "gblock", 1, 0.95, source=lsource, fill_alpha=0.6, color="color")
        f.outline_line_color = None
        f.grid.grid_line_color = None
        f.axis.axis_line_color = None
        f.axis.major_tick_line_color = None
        f.axis.major_label_standoff = 0
        f.xaxis.visible = False

        legends.append(f)
    return legends


def draw_heatmap(reads_df, color_map):
    tidy_df = reads_df.stack().to_frame().reset_index()
    tidy_df.columns = ["read", "pos", "gblock"]
    tidy_df["color"] = [color_map[p][g] for p, g in zip(tidy_df.pos, tidy_df.gblock)]
    tidy_df.to_csv("temp.tsv", sep="\t")
    tidy_df = tidy_df.applymap(str)
    cds = ColumnDataSource(tidy_df)
    hover1 = HoverTool(tooltips=[("gBlock", "@gblock"),("read","@read")])
    fig = figure(
        tools = [hover1],
        plot_width=300 * len(reads_df.columns),
        plot_height=500,  # 10*len(reads_df),
        title="Full Length Reads",
        x_range=list(reads_df.columns),
        y_range=[str(i) for i in reversed(range(len(reads_df)))],
    )

    fig.rect("pos", "read", 1, 0.95, source=cds, fill_alpha=0.6, color='color')
    fig.outline_line_color = None
    fig.grid.grid_line_color = None
    fig.axis.axis_line_color = None
    fig.axis.major_tick_line_color = None
    fig.axis.major_label_standoff = 0
    fig.yaxis.visible = False
    return fig


def build_heatmap(counts):
    # shape nreads by ncomponents
    reads_df = counts_to_flat(counts)
    # dict: comp_pos -> {component -> color}
    color_map = get_colors(reads_df)
    legends = build_legends(reads_df, color_map)
    heat_fig = draw_heatmap(reads_df, color_map)
    # legends.append(heat_fig)
    return json_item(column([row(legends), heat_fig]))


def main(counts_path, outpath):
    """ makes a pie chart, and a heatmap thing and dumps them to html
    """
    counts = load_read_types(counts_path)
    pie_json = build_pie_chart(counts)
    heat_json = build_heatmap(counts)
    html_text = html_template.format(
        cdn=CDN.render(),
        pie_chart_json=json.dumps(pie_json),
        heatmap_json=json.dumps(heat_json)
    )
    with open(outpath, "w") as fout:
        fout.write(html_text)


def snakemain():
    counts = snakemake.input[0]
    outpath = snakemake.output[0]
    main(counts, outpath)


if(__name__ == "__main__"):
    snakemain()